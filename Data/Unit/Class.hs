module Data.Unit.Class where

class IsUnit u where
  constU :: a -> (u -> a)
  constU = const

  unconstU :: (u -> a) -> a
  unconstU = ($unit)

  unit :: u
  unit = unconstU id

instance IsUnit () where
  unit = ()

