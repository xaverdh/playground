{-# language MultiParamTypeClasses, FlexibleInstances #-}
module Data.SRing where

class SRing r where
  add :: r -> r -> r
  zero :: r

  mult :: r -> r -> r
  one :: r

scale :: (SRing r,Functor f) => r -> f r -> f r
scale r = fmap (mult r)

instance SRing Integer where
  add = (+)
  zero = 0

  mult = (*)
  one = 1

class (SRing r,SRing a) => Algebra r a where
  act :: r -> a

instance SRing r => Algebra r r where
  act = id

