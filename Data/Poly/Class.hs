{-# language
   MultiParamTypeClasses
 , FunctionalDependencies
 , RankNTypes
 , ConstraintKinds #-}
module Data.Poly.Class where

import Data.SRing
import Data.Unit.Class


class Algebra r p => Poly r v p | p -> r, p -> v where
  polyFoldMap :: Algebra r s => (v -> s) -> p -> s
  polyFoldMap = flip polyApp

  polyApp :: Algebra r s => p -> (v -> s) -> s
  polyApp = flip polyFoldMap

  polyUnfoldMap :: Algebra r s => (p -> s) -> v -> s
  polyUnfoldMap f = f . varPoly

  varPoly :: Poly r v p => v -> p
  varPoly = polyUnfoldMap id

evalPoly :: (Algebra r s,Poly r v p) => p -> (v -> s) -> s
evalPoly p vals = polyApp p vals

constPoly :: Poly r v p => r -> p
constPoly = act

polyIsPoly :: (Poly r v q,Poly r v p) => q -> p
polyIsPoly = polyFoldMap varPoly

type PolyU r u p = (IsUnit u,Poly r u p)

evalPolyU :: (Algebra r s,PolyU r u p) => p -> s -> s
evalPolyU p s = evalPoly p (constU s)

