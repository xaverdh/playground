{-# language
   FlexibleInstances
 , MultiParamTypeClasses #-}
module Data.Poly.Sym where

import Data.Poly.Class
import Data.SRing

data PolyS r v =
  PolyConst r
  | PolyVar v
  | PolySum (PolyS r v) (PolyS r v)
  | PolyMult (PolyS r v) (PolyS r v)

instance SRing r => SRing (PolyS r v) where
  add = PolySum
  zero = PolyConst zero
  mult = PolyMult
  one = PolyConst one

instance SRing r => Algebra r (PolyS r v) where
  act = PolyConst

instance SRing r => Poly r v (PolyS r v) where
  polyApp p f = case p of
    PolyConst r -> act r
    PolyVar v -> f v
    PolySum p1 p2 -> add (polyApp p1 f) (polyApp p2 f)
    PolyMult p1 p2 -> mult (polyApp p1 f) (polyApp p2 f)
  varPoly = PolyVar

