{-# language
   FlexibleInstances
 , MultiParamTypeClasses
 , DeriveTraversable #-}

module Data.Poly.List where

import Data.Poly.Class
import Data.Unit.Class
import Data.SRing

import Data.Sequence as S


newtype PolyL u r = PolyL {  unpolyL :: S.Seq r } deriving
  (Functor,Foldable,Traversable,Eq,Ord)

shiftAddPolyL :: SRing r => r -> PolyL u r -> PolyL u r
shiftAddPolyL r (PolyL f) = PolyL (r<|f)

constPolyL :: r -> PolyL u r
constPolyL = PolyL . S.singleton

linearPolyL :: SRing r => r -> r -> PolyL u r
linearPolyL a b = shiftAddPolyL a (act b)

instance SRing r => SRing (PolyL u r) where
  add (PolyL f) (PolyL g) = case f of
    Empty -> PolyL g
    a:<|as -> case g of
      Empty -> PolyL f
      b:<|bs -> shiftAddPolyL (add a b) $ add (PolyL as) (PolyL bs)
  zero = constPolyL zero
  mult (PolyL f) (PolyL g) = case f of
    Empty -> zero
    (a :<| as) -> case g of
      Empty -> zero
      (b :<| bs) ->
        shiftAddPolyL (mult a b)
        $ scale b (PolyL as) `add` scale a (PolyL bs)
        `add` shiftAddPolyL zero
          (mult (PolyL as) (PolyL bs))
  one = constPolyL one

instance SRing r => Algebra r (PolyL u r) where
  act = constPolyL

instance (SRing r,IsUnit u) => Poly r u (PolyL u r) where
  polyFoldMap h (PolyL f) = ev f
    where
      var = h unit
      ev Empty = zero
      ev (r:<|rs) = add (act r) $ mult var (ev rs)
  varPoly = const $ linearPolyL zero one


