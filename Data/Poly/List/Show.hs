{-# language
   ScopedTypeVariables #-}

module Data.Poly.List.Show where

import Data.Unit.Class
import Data.Poly.List
import Data.SRing

import Data.Sequence as S


instance (Eq r,SRing r,Show r, Show u,IsUnit u) => Show (PolyL u r) where
  show (PolyL Empty) = show (zero::r)
  show (PolyL (r:<|rs)) = foldr (<>) "" $ S.intersperse " + "
    $ triage r (show r) <> unroll 1 rs
    where
      triage :: r -> String -> S.Seq String
      triage r t = if r == zero then S.empty else S.singleton t

      unroll :: Integer -> S.Seq r -> S.Seq String
      unroll _ Empty = S.empty
      unroll k (r:<|rs) =
        triage r (show r <> " * " <> var k)
        <> unroll (k+1) rs

      var k = show (unit::u) <> "^" <> show k

