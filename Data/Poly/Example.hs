module Data.Poly.Example where

import Data.SRing
import Data.Poly.Class
import Data.Poly.List
import Data.Poly.List.Show
import Data.Poly.Sym
import Data.Poly.Sym.Show
import Data.Unit.Class

import qualified Data.Sequence as S

data X = X deriving (Show)

instance IsUnit X where
  unit = X

type P = PolyL X Integer

x = varPoly X :: P

t1 = PolyL (S.fromList [0,15,2,-1]) :: P

p = linearPolyL 5 (-1) :: P
q = linearPolyL 3 1 :: P

t2 = p `mult` q `mult` x

main = do
  print t1
  print t2
  print ( polyIsPoly t1 :: PolyS Integer X )
  print $ map (evalPolyU t1) ([-6..6]::[Integer])

