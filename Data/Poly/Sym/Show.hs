
module Data.Poly.Sym.Show where

import Data.Poly.Sym

instance (Show r,Show v) => Show (PolyS r v) where
  show p = case p of
    PolyConst r -> show r
    PolyVar v -> show v
    PolySum p1 p2 -> "(" <> show p1 <> " + " <> show p2 <> ")"
    PolyMult p1 p2 -> show p1 <> " * " <> show p2

