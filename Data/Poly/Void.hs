{-# language
   FlexibleInstances
 , MultiParamTypeClasses #-}

module Data.Poly.Void where

import Data.Poly.Class
import Data.SRing

import Data.Void

data PolyV r = PolyV r

instance SRing r => SRing (PolyV r) where
  add (PolyV a) (PolyV b) = PolyV $ add a b
  zero = PolyV zero

  mult (PolyV a) (PolyV b) = PolyV $ mult a b
  one = PolyV one

instance SRing r => Algebra r (PolyV r) where
  act = PolyV

instance SRing r => Poly r Void (PolyV r) where
  polyFoldMap _ (PolyV r) = act r
  varPoly = absurd

