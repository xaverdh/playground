{-# language
   RankNTypes
 , MultiParamTypeClasses
 , FlexibleInstances #-}
module Data.Poly.Func where

import Data.Poly.Class
import Data.SRing

data PolyF r v = PolyF { unpolyF :: forall s. Algebra r s => (v -> s) -> s }

instance SRing r => SRing (PolyF r v) where
  add (PolyF f) (PolyF g) = PolyF $ \h -> add (f h) (g h)
  zero = PolyF $ const zero
  mult (PolyF f) (PolyF g) = PolyF $ \h -> mult (f h) (g h)
  one = PolyF $ const one

instance SRing r => Algebra r (PolyF r v) where
  act r = PolyF $ \f -> act r

instance SRing r => Poly r v (PolyF r v) where
  polyApp = unpolyF
  varPoly v = PolyF ($v)

